#!/usr/bin/env python3
"""
Program to extract histogram information from a set of images into a CSV file

Requires Python 3.4 or greater with the Pillow package installed.
"""
import argparse
from PIL import Image, ImageStat
import pathlib
import csv

__licence__ = """
Copyright 2016 Christopher Corpora (pyhasher@gmail.com)

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""

__version__ = "1.0"

__author__ = "Chris Corpora"

DEFAULT_FILENAME = 'image_stats.csv'

def mk_header(img):
    header = ['Filename', 'Std Dev', 'Mean', 'Median']
    bands = img.getbands()
    # histogram is in order of R 0 -> 255, G 0 -> 255, B 0 -> 255
    for i in range(len(bands)):
        header += [bands[i] + str(j) for j in range(256)]    
    return header

def setup(args):
    dirpath = pathlib.Path(args.dirpath)
    if args.output:
        output = pathlib.Path(args.output)
        if output.exists() and output.is_dir():
            output = output / DEFAULT_FILENAME
    else:
        output = pathlib.Path() / DEFAULT_FILENAME
    if args.selection_area:
        selection_area = tuple(args.selection_area)
    else:
        selection_area = None 
    return dirpath, output, selection_area
    
def run(args):
    dirpath, output, selection_area = setup(args)
    header = None
    # csv package handles newlines automatically
    with output.open('w', newline='') as log:
        csv_writer = csv.writer(log)
        for fpath in dirpath.iterdir():
            if fpath.match(args.pattern):
                img = Image.open(fpath.as_posix())
                # use image data to write header
                if not header:
                    header = mk_header(img)
                    csv_writer.writerow(header)
                # if a selection area is specified, crop image to area and then get stats
                if selection_area:
                    img = img.crop(box=selection_area)
                stats = ImageStat.Stat(img)
                row = [fpath.name, tuple(stats.stddev), tuple(stats.mean), tuple(stats.median)]
                row += img.histogram()
                csv_writer.writerow(row)
            

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('dirpath', help='Path to directory containing image files')
    parser.add_argument('-o', '--output', metavar='<FILE>|<DIR>',
                        help='Path to logfile. Can be a directory or filepath.'
                        ' If not specified, will be automatically generated in the current working directory.')
    parser.add_argument('-s', '--selection-area', type=int, nargs=4,
                        metavar='INT',
                        help='Selection area to use; needs 4 positive integers:'
                        ' start_x start_y width height. If not specified, the entire image is used.')
    parser.add_argument('-p', '--pattern', type=str,
                        help='Filename pattern to match such as "*.png" for all PNG image files.'
                        'This is not case-sensitve.')
    args = parser.parse_args()
    run(args)